let title = document.createElement('span')
title.innerHTML = 'KATAS 3'
document.body.appendChild(title)
document.write('<br>')

function counting1() {

  // função exibe os numeros de 1 a 25
  for (let i = 1; i <= 25; i++) {
    console.log(i)

    let x = document.createElement('span')
    x.innerHTML = i + ", "
    x.id = "counting1"
    document.body.appendChild(x)
  }
}
counting1()
document.write('<br>')
function counting2() {
  // função exibe os numeros de 25 ate 1
  for (let i = 25; i >= 1; i--) {
    let x = document.createElement('span')
    x.innerHTML = i + ", "
    x.id = "counting2"
    document.body.appendChild(x)
  }
}
counting2()
document.write('<br>')
function counting3() {
  //função exibe os números de -1 até -25
  for (let i = -1; i >= -25; i--) {
    console.log(i)
    let x = document.createElement('span')
    x.innerHTML = i + ", "
    x.id = "counting3"
    document.body.appendChild(x)
  }
}
counting3()
document.write('<br>')
function counting4() {
  //função exibe os números de -25 até -1
  for (let i = -25; i <= -1; i++) {
    console.log(i)
    let x = document.createElement('span')
    x.innerHTML = i + ", "
    x.id = "counting4"
    document.body.appendChild(x)
  }
}
counting4()
document.write('<br>')
function counting5() {
  // função exibe os números ímpares de 25 até -25
  for (let i = 25; i >= -25; i--) {
    if ((i % 2) !== 0) {
      console.log(i)
    let x = document.createElement('span')
    x.innerHTML = i + ", "
    x.id = "counting5"
    document.body.appendChild(x)
    }
  }
}
counting5()
document.write('<br>')
function counting6() {
  //função exibe os números de 1 até 100 divisiveis/multiplos de 3
  for (let i = 1; i <= 100; i++) {
    if ((i % 3) === 0) {
      console.log(i)
      let x = document.createElement('span')
      x.innerHTML = i + ", "
      x.id = "counting6"
      document.body.appendChild(x)
    }
  }
}
counting6()
document.write('<br>')
function counting7() {
  // função exibe os números de 1 até 100 diviseis/multiplos de 7
  for (let i = 1; i <= 100; i++) {
    if ((i % 7) === 0) {
      console.log(i)
      let x = document.createElement('span')
      x.innerHTML = i + ", "
      x.id = "counting7"
      document.body.appendChild(x)
    }
  }
}
counting7()
document.write('<br>')
//função exibe ps números de 100 até 1, divisiveis por 3 ou 7
function counting8() {
  for (let i = 100; i >= 1; i--) {
    if ((i % 3) === 0 || (i % 7) === 0) {
      console.log(i)
      let x = document.createElement('span')
      x.innerHTML = i + ", "
      x.id = "counting8"
      document.body.appendChild(x)
    }
  }
}
counting8()
document.write('<br>')
//função exibe os números ímpares e divisiveis por 5
function counting9() {
  for (let i = 5; i <= 100; i++) {
    if ((i % 2) !== 0 && (i % 5) === 0) {
      console.log(i)
      let x = document.createElement('span')
      x.innerHTML = i + ", "
      x.id = "counting9"
      document.body.appendChild(x)
    }
  }
}
counting9()
document.write('<br>')


const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
function arrays1(sampleArray) {
  for (let i = 0; i < sampleArray.length; i++) {
    // todos os números
    let x = document.createElement('span')
    x.id = 'arrays1'
    x.innerHTML = sampleArray[i] + ", "
    document.body.appendChild(x)
  }
}
arrays1(sampleArray)
document.write('</br>')
function arrays2(sampleArray) {
  for (let i of sampleArray) {
    // let i of é para pegar somente elementos dentro do indice
    if (([i] % 2) === 0) {
      // retornar números pares
      console.log(i)
      let x = document.createElement('span')
      x.id = 'arrays2'
      x.innerHTML = i + ", "
      document.body.appendChild(x)
    }
  }
}
arrays2(sampleArray)
document.write('<br>')
//arrays2(sampleArray)
//retorna os números nos índices(posição) par
//for (let i = 0; i <= sampleArray.length; i++) {
//if ((i % 2) === 0)
//console.log(sampleArray[i])
// }
//}
//arrays2(sampleArray)
function arrays3(sampleArray) {
  for (let i of sampleArray) {
    if (([i] % 2) !== 0) {
      console.log(i)
      let x = document.createElement('span')
      x.id = 'arrays3'
      x.innerHTML = [i] + ", "
      document.body.appendChild(x)
    }
  }
}
arrays3(sampleArray)
document.write('<br>')
function arrays4(sampleArray) {
  for (let i of sampleArray) {
    if (([i] % 8) === 0) {
    console.log(i)
    let x = document.createElement('span')
      x.id = 'arrays4'
      x.innerHTML = i + ", "
      document.body.appendChild(x)
    }
  }
}
arrays4(sampleArray)
document.write('<br>')
function arrays5(sampleArray) {
  for (let i of sampleArray) {
    console.log(i * i)
    let x = document.createElement('span')
      x.id = 'arrays2'
      x.innerHTML = (i * i) + ", "
      document.body.appendChild(x)
  }
}
arrays5(sampleArray)
document.write('<br>')
//function arrays6(sampleArray) {
// for (let i = 0,  total = 0; i < sampleArray.length; total += sampleArray[i++]); 
//  console.log(total);
// }
//arrays6(sampleArray)
function somatorio(a, b) {
  let result = 0
  for (i = a; i <= b; i++) {
    result += i
  }
  return result
}
let my_value = somatorio(1, 20)
console.log(my_value)

let v = document.createElement('span')
v.innerHTML = my_value
v.id = "somatorio"
document.body.appendChild(v)
document.write('<br>')
function arrays7(sampleArray) {
  let tamanho = sampleArray.length;
  let total = 0;
  while (tamanho--) {
    total += sampleArray[tamanho];
  }
  return total
}
let my_value_a = arrays7(sampleArray)
console.log(my_value_a)

let u = document.createElement('span')
u.innerHTML = my_value_a
u.id = "array_7"
document.body.appendChild(u)
document.write('<br>')
function arrays8(sampleArray) {
  let menor = sampleArray[0]
  for (i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] < menor)
      menor = sampleArray[i]
  }
  return menor
}
let my_value_b = arrays8(sampleArray)
console.log(my_value_b)

let z = document.createElement('span')
z.innerHTML = my_value_b
z.id = "array_8"
document.body.appendChild(z)
document.write('<br>')
function arrays9(sampleArray) {
  let maior = sampleArray[0]
  for (i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] > maior)
      maior = sampleArray[i]
  }
  return maior
}
let my_value_c = arrays9(sampleArray)
console.log(my_value_c)

let x = document.createElement('span')
x.innerHTML = my_value_c
x.id = "array_9"
document.body.appendChild(x)